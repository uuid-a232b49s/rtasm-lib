#ifndef HEADER_RTASM_COMPILATION
#define HEADER_RTASM_COMPILATION 1

#include <memory>

#include "common.hpp"
#include "definition.hpp"
#include "compilation.details.inl"

namespace rtasm {

    struct Method {
        std::shared_ptr<void> userData;
        template<typename signature_t>
        auto cast() const -> typename details::methodCast_t<signature_t>::fnPtr {
            using c = details::methodCast_t<signature_t>;
            return reinterpret_cast<typename c::fnPtr>(cast0(c::sig(), c::argc()));
        }
        virtual auto pointer() const noexcept -> void * = 0;
    private:
        virtual void * cast0(const details::typeMeta_t *, usize) const = 0;
    };

    struct Assembly {
        std::shared_ptr<void> userData;
        virtual usize methodc() const noexcept = 0;
        virtual const Method * methodv(usize) const = 0;
        virtual ~Assembly() = default;
    };

    std::unique_ptr<const Assembly> compile(const IAssemblyDef &);

}

#endif
