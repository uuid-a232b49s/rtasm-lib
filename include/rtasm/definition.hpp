#ifndef HEADER_RTASM_DEFINITION
#define HEADER_RTASM_DEFINITION 1

#include <memory>
#include <cstdint>
#include <typeinfo>
#include <type_traits>

#include "common.hpp"
#include "opcode.hpp"

namespace rtasm {

    struct ITypeDef;

    struct TypeDefMeta {
        const ITypeDef * type;
        usize indirection;
        TypeDefMeta(decltype(type) type = nullptr, decltype(indirection) indirection = 0)
            : type(type), indirection(indirection) {}
        friend bool operator==(const TypeDefMeta & a, const TypeDefMeta & b) noexcept;
    };

    struct ITypeDef {
        struct SInt;
        struct UInt;
        struct Float;
        struct Void;
        struct Extern;

        operator TypeDefMeta () const noexcept { return TypeDefMeta(this, 0); }

        virtual unsigned char id() const = 0;
        virtual usize size() const = 0;
        virtual usize align() const = 0;

        virtual ~ITypeDef() = default;

        template<typename>
        static const ITypeDef * fromType() noexcept;

    private:
        RTASM_DECL_5_DEFAULT(ITypeDef);
    };

    struct ITypeDef::SInt : ITypeDef {
        static const SInt Instance;
        using NativeType = std::int64_t;
        static constexpr unsigned char Id = 2;
        virtual unsigned char id() const override { return Id; }
        virtual usize size() const override { return sizeof(NativeType); }
        virtual usize align() const override { return alignof(NativeType); }
    private:
        SInt() = default;
        RTASM_DECL_4_DELETED(SInt);
    };

    struct ITypeDef::UInt : ITypeDef {
        static const UInt Instance;
        using NativeType = std::uint64_t;
        static constexpr unsigned char Id = 3;
        virtual unsigned char id() const override { return Id; }
        virtual usize size() const override { return sizeof(NativeType); }
        virtual usize align() const override { return alignof(NativeType); }
    private:
        UInt() = default;
        RTASM_DECL_4_DELETED(UInt);
    };

    struct ITypeDef::Float : ITypeDef {
        static const Float Instance;
        using NativeType = double;
        static constexpr unsigned char Id = 4;
        virtual unsigned char id() const override { return Id; }
        virtual usize size() const override { return sizeof(NativeType); }
        virtual usize align() const override { return alignof(NativeType); }
    private:
        Float() = default;
        RTASM_DECL_4_DELETED(Float);
    };

    struct ITypeDef::Void : ITypeDef {
        static const Void Instance;
        using NativeType = void;
        static constexpr unsigned char Id = 1;
        virtual unsigned char id() const override { return Id; }
        virtual usize size() const override { return 0; }
        virtual usize align() const override { return 0; }
    private:
        Void() = default;
        RTASM_DECL_4_DELETED(Void);
    };

    struct ITypeDef::Extern : ITypeDef {
        static constexpr unsigned char Id = 5;
        virtual unsigned char id() const override { return Id; }
        virtual usize size() const = 0;
        virtual usize align() const = 0;
        virtual const std::type_info * typeinfo() const = 0;
    protected:
        RTASM_DECL_5_DEFAULT(Extern);
    };

    struct IMethodDef {
        virtual const char * identifier() const { return nullptr; }

        virtual usize argc() const { return 0; }
        virtual TypeDefMeta argv(usize) const { return nullptr; }

        virtual TypeDefMeta ret() const { return ITypeDef::Void::Instance; }

        virtual usize opcodec() const { return 0; }
        virtual OpCode opcodev(usize) const { return OpCode(); }

        virtual std::shared_ptr<void> userData() const { return std::shared_ptr<void>(); }

        virtual ~IMethodDef() = default;
    protected:
        RTASM_DECL_5_DEFAULT(IMethodDef);
    };

    struct IAssemblyDef {
        virtual usize methodc() const { return 0; };
        virtual const IMethodDef * methodv(usize) const { return nullptr; }

        virtual std::shared_ptr<void> userData() const { return std::shared_ptr<void>(); }

        virtual ~IAssemblyDef() = default;
    protected:
        RTASM_DECL_5_DEFAULT(IAssemblyDef);
    };

    namespace types {
        using Void = typename ITypeDef::Void;
        using SInt = typename ITypeDef::SInt;
        using UInt = typename ITypeDef::UInt;
        using Float = typename ITypeDef::Float;
    }

    namespace ntypes {
        using Void = typename types::Void::NativeType;
        using SInt = typename types::SInt::NativeType;
        using UInt = typename types::UInt::NativeType;
        using Float = typename types::Float::NativeType;
    }

}

namespace rtasm {
    template<typename x>
    const ITypeDef * ITypeDef::fromType() noexcept {
        static_assert(!std::is_const<x>::value, "base type can not be const");
        static_assert(!std::is_volatile<x>::value, "base type can not be volatile");
        static_assert(!std::is_pointer<x>::value, "base type can not be a pointer");
        static_assert(!std::is_reference<x>::value, "base type can not be a reference");
        struct impl_t final : ITypeDef::Extern {
            virtual usize size() const override { return sizeof(x); }
            virtual usize align() const override { return alignof(x); }
            virtual const std::type_info * typeinfo() const override { return &typeid(x); }
        } static const impl;
        return &impl;
    }
    template<>
    inline const ITypeDef * ITypeDef::fromType<typename ITypeDef::Void::NativeType>() noexcept { return &ITypeDef::Void::Instance; }
    template<>
    inline const ITypeDef * ITypeDef::fromType<typename ITypeDef::SInt::NativeType>() noexcept { return &ITypeDef::SInt::Instance; }
    template<>
    inline const ITypeDef * ITypeDef::fromType<typename ITypeDef::UInt::NativeType>() noexcept { return &ITypeDef::UInt::Instance; }
    template<>
    inline const ITypeDef * ITypeDef::fromType<typename ITypeDef::Float::NativeType>() noexcept { return &ITypeDef::Float::Instance; }
}

#endif
