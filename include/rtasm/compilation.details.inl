// include compilation.hpp

#if !defined(HEADER_RTASM_COMPILATION) || defined(HEADER_RTASM_COMPILATION_DETAILS)
#error illegal include
#endif
#define HEADER_RTASM_COMPILATION_DETAILS 1

#include <typeindex>
#include <type_traits>

#include "definition.hpp"

namespace rtasm {
    namespace details {

        using typeIdType = typename std::remove_const<decltype(ITypeDef::Void::Id)>::type;

        struct typeMeta_t {
            const std::type_info * typeInfo;
            usize indirection;
            typeIdType typeId;
        };

        template<typename x>
        struct typeToId : std::conditional<
            std::is_same<x, typename ITypeDef::SInt::NativeType>::value, std::integral_constant<typeIdType, ITypeDef::SInt::Id>,
                typename std::conditional<
                    std::is_same<x, typename ITypeDef::UInt::NativeType>::value, std::integral_constant<typeIdType, ITypeDef::UInt::Id>,
                    typename std::conditional<
                        std::is_same<x, typename ITypeDef::Float::NativeType>::value, std::integral_constant<typeIdType, ITypeDef::Float::Id>,
                        typename std::conditional<
                            std::is_same<x, typename ITypeDef::Void::NativeType>::value, std::integral_constant<typeIdType, ITypeDef::Void::Id>,
                            std::integral_constant<typeIdType, ITypeDef::Extern::Id>
                        >::type
                    >::type
                >::type
        >::type {};

        template<typename x, usize ind_>
        struct toPtrBase { using type = typename std::remove_cv<x>::type; static constexpr usize ind = ind_; };
        template<typename x, usize ind_>
        struct toPtrBase<x *, ind_> : toPtrBase<x, ind_ + 1> {};

        template<typename x>
        struct typeToMeta {
            static typeMeta_t value() noexcept {
                using base = toPtrBase<x, 0>;
                return typeMeta_t { &typeid(typename base::type), base::ind, typeToId<typename base::type>::value };
            }
        };

        template<typename>
        struct methodCast_t {};

        template<typename ret_, typename ... args_>
        struct methodCast_t<ret_(args_...)> {
            using fnPtr = ret_(*)(args_...);
            static constexpr usize argc() noexcept { return sizeof...(args_); }
            static const typeMeta_t * sig() noexcept {
                static const typeMeta_t a[] { typeToMeta<ret_>::value(), typeToMeta<args_>::value()... };
                return a;
            }
        };

        template<typename ret_, typename ... args_>
        struct methodCast_t<ret_(*)(args_...)> : methodCast_t<ret_(args_...)> {};

    }
}
