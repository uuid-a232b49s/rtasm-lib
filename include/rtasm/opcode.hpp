#ifndef HEADER_RTASM_OPCODE
#define HEADER_RTASM_OPCODE 1

#include "common.hpp"

namespace rtasm {

    struct OpCode {
        enum struct code_t {
            null,
            arg,
            ld_const,
            add,
            sub,
            jmp,
            jmp_cond,
            ret,
            lbl
        } m_code = code_t::null;
        union {
            usize u;
            struct { usize a, b; } uu;
        } m_data = {};
        const char * m_name = "";

        static OpCode arg(usize argIdx) noexcept;
        static OpCode add() noexcept;
        static OpCode ret() noexcept;
        static OpCode jmp(usize lblId) noexcept;
        static OpCode br(bool onTrue, usize lblId) noexcept;
        static OpCode label(usize lblId) noexcept;

    };

}

#endif
