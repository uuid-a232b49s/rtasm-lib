#ifndef HEADER_RTASM_EXCEPTION
#define HEADER_RTASM_EXCEPTION 1

#include <exception>

#include "common.hpp"

namespace rtasm {

    class Exception : public std::exception {
        virtual const char * file() const noexcept = 0;
        virtual usize line() const noexcept = 0;
    };

    class NullPtrException : public Exception {};

    class CompilationException : public Exception {
        virtual isize opcodeIdx() const noexcept = 0;
    };

}

#endif
