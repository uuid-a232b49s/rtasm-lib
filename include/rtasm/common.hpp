#ifndef HEADER_RTASMLIB_COMMON
#define HEADER_RTASMLIB_COMMON 1

namespace rtasm {

    using isize = signed long long;
    using usize = unsigned long long;

}

#define RTASM_DECL_4_(x, a) \
x(const x &) = a; \
x(x &&) = a; \
x & operator=(const x &) = a; \
x & operator=(x &&) = a;

#define RTASM_DECL_4_DELETED(x) RTASM_DECL_4_(x, delete)
#define RTASM_DECL_4_DEFAULT(x) RTASM_DECL_4_(x, default)
#define RTASM_DECL_5_DEFAULT(x) x() = default; RTASM_DECL_4_(x, default)

#endif
