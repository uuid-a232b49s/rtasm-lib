#ifndef HEADER_RTASM_DEFINITION_BASIC
#define HEADER_RTASM_DEFINITION_BASIC 1

#include <memory>
#include <vector>
#include <string>
#include <typeinfo>

#include "definition.hpp"

namespace rtasm {

    struct BasicTypeExternDef : ITypeDef::Extern {
        usize m_size = 0, m_align = 0;
        const std::type_info * m_typeinfo = nullptr;
        virtual usize size() const override { return m_size; }
        virtual usize align() const override { return m_align; }
        virtual const std::type_info * typeinfo() const override { return m_typeinfo; }
    };

    struct BasicMethodDef : IMethodDef {
        std::string m_identifier;
        virtual const char * identifier() const override { return m_identifier.c_str(); }

        std::vector<TypeDefMeta> m_args;
        virtual usize argc() const override { return m_args.size(); }
        virtual TypeDefMeta argv(usize i) const override { return m_args.at(i); }

        TypeDefMeta m_ret = ITypeDef::Void::Instance;
        virtual TypeDefMeta ret() const override { return m_ret; }

        std::vector<OpCode> m_opcodes;
        virtual usize opcodec() const override { return m_opcodes.size(); }
        virtual OpCode opcodev(usize i) const override { return m_opcodes.at(i); }

        std::shared_ptr<void> m_userData;
        virtual std::shared_ptr<void> userData() const override { return m_userData; }
    };

    struct BasicAssemblyDef : IAssemblyDef {
        std::vector<std::unique_ptr<IMethodDef>> m_methods;
        virtual usize methodc() const { return m_methods.size(); };
        virtual const IMethodDef * methodv(usize i) const { return m_methods.at(i).get(); }

        std::shared_ptr<void> m_userData;
        virtual std::shared_ptr<void> userData() const override { return m_userData; }
    };

}

#endif
