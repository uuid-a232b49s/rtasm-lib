cmake_minimum_required(VERSION 3.4)
cmake_policy(VERSION 3.4)

list(APPEND CMAKE_MESSAGE_INDENT "[platform] ")

# find architecture
# RTASM_TARGET_ARCH
#########################################################
string(TOLOWER ${CMAKE_SYSTEM_PROCESSOR} RTASM_TARGET_ARCH)
if("${RTASM_TARGET_ARCH}" MATCHES "^(amd64|i686|x86_64)$")
    set(RTASM_TARGET_ARCH x64)
else()
    message(FATAL_ERROR "unsupported architecture: ${CMAKE_SYSTEM_PROCESSOR}")
endif()

# find compiler
# https://cmake.org/cmake/help/latest/variable/CMAKE_LANG_COMPILER_ID.html
# RTASM_TARGET_COMP
#########################################################
string(TOLOWER ${CMAKE_CXX_COMPILER_ID} RTASM_TARGET_COMP)

# find os
# RTASM_TARGET_OS
#########################################################
string(TOLOWER ${CMAKE_SYSTEM_NAME} RTASM_TARGET_OS)
if("${RTASM_TARGET_OS}" MATCHES "^(linux|darwin|ios|macosx|unix|posix)$")
    set(RTASM_TARGET_OS posix)
elseif("${RTASM_TARGET_OS}" MATCHES "^(windows)$")
    set(RTASM_TARGET_OS win32)
else()
    message(FATAL_ERROR "unsupported os: ${CMAKE_SYSTEM_NAME}")
endif()

message(STATUS "target architecture: ${RTASM_TARGET_ARCH}")
message(STATUS "target compiler: ${RTASM_TARGET_COMP}")
message(STATUS "target os: ${RTASM_TARGET_OS}")

# finds platform matching dir paths
# ROOT_PATH: dir in which to search for platform subdirs
# OUTPUT: output variable (dir list)
function(rtasm_platform_paths ROOT_PATH OUTPUT)
    set(_platform_dirs "")
    set(_platform_identifiers "${RTASM_TARGET_ARCH};${RTASM_TARGET_OS};${RTASM_TARGET_COMP}")
    file(GLOB _paths LIST_DIRECTORIES TRUE "${ROOT_PATH}/*") # get all dirs
    foreach(_dir IN LISTS _paths)
        if(IS_DIRECTORY "${_dir}") # filter for dirs
            get_filename_component(_dir_identifiers "${_dir}" NAME) # get dir name
            string(REPLACE "." ";" _dir_identifiers "${_dir_identifiers}") # convert dir name to a list (x64.posix.clang -> [x64, posix, clang])
            set(_ok TRUE)
            foreach(_dir_identifier IN LISTS _dir_identifiers) # iterate over each id in the dir name
                if(NOT "${_dir_identifier}" IN_LIST _platform_identifiers) # if dir does not specify one of the currently configured value - skip it
                    set(_ok FALSE) # mark as fail
                    break() # early exit
                endif()
            endforeach()
            if(${_ok}) # did dir pass based on ${_platform_identifiers}
                list(APPEND _platform_dirs "${_dir}")
            endif()
        endif()
    endforeach()
    set(${OUTPUT} "${_platform_dirs}" PARENT_SCOPE)
endfunction()

# finds platform matching file paths
# ROOT_PATH: dir in which to search for platform subdirs
# RECURSE: whether to recurse file search
# FILE_FILTERS: globbing file filter
# OUTPUT: output variable (file list)
function(rtasm_platform_sources ROOT_PATH RECURSE FILE_FILTERS OUTPUT)
    set(_glob_exp "")
    foreach(_ff IN LISTS FILE_FILTERS)
        list(APPEND _glob_exp "@PATH@/${_ff}")
    endforeach()

    if(${RECURSE})
        set(_glob_recurse GLOB_RECURSE)
    else()
        set(_glob_recurse GLOB)
    endif()

    set(_output "")

    rtasm_platform_paths("${ROOT_PATH}" _paths)
    foreach(_path IN LISTS _paths)
        foreach(_ff IN LISTS FILE_FILTERS)
            file(${_glob_recurse} _s "${_path}/${_ff}")
            list(APPEND _output "${_s}")
        endforeach()
    endforeach()

    set(${OUTPUT} "${_output}" PARENT_SCOPE)
endfunction()

list(POP_BACK CMAKE_MESSAGE_INDENT)
