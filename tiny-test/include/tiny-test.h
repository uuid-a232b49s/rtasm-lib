#ifndef HEADER_TINYTEST_TEST
#define HEADER_TINYTEST_TEST 1

#if TINYTEST_TEST
    #ifdef __cplusplus
    #include <cassert>
    #define TINYTEST_LINKAGE extern "C"
    #define TINYTEST_NOEXCEPT noexcept
    #else
    #include <assert.h>
    #define TINYTEST_LINKAGE
    #define TINYTEST_NOEXCEPT
    #endif
#endif

#if TINYTEST_TEST
    #define ASSERT_EQ(condition) assert((condition))

    #define TEST(IDENTIFIER) TEST0(IDENTIFIER, TINYTEST_TARGET)
    #define TEST0(IDENTIFIER, TGT) TEST1(IDENTIFIER, TGT)
    #define TEST1(IDENTIFIER, TGT) \
    TINYTEST_LINKAGE void TINYTEST_ ## TGT ## _ ## IDENTIFIER () TINYTEST_NOEXCEPT 
#else
    #define ASSERT_EQ(condition)
    #define TEST(IDENTIFIER) [[maybe_unused]] static void TINYTEST_ ## IDENTIFIER ()
#endif

#endif
