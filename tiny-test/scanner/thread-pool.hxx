#ifndef HEADER_THREADPOOL
#define HEADER_THREADPOOL 1

#include <mutex>
#include <atomic>
#include <memory>
#include <vector>
#include <future>
#include <thread>
#include <utility>
#include <exception>
#include <functional>
#include <condition_variable>

namespace rtasm {

    struct thread_pool {
    private:
        struct thread_data {
            std::thread thread;
        };

        struct task_t {
            std::function<void(void)> fn;
        };

        template<typename x>
        struct array_deleter_t { void operator()(x * a) const { delete[] a; } };

        using ull = unsigned long long;

        std::unique_ptr<thread_data, array_deleter_t<thread_data>> m_threadv;
        ull m_threadc;

        std::mutex m_mtx;
        std::condition_variable m_cnd;
        mutable std::mutex m_mtxAwait;
        mutable std::condition_variable m_cndAwait;
        std::vector<task_t> m_tasks;
        ull m_taskIdx = 0;
        bool m_running = false;

        std::atomic_ullong m_scheduledTasks;
        std::atomic_ullong m_activeTasks;

    public:

        std::function<void(std::exception_ptr)> exceptionHandler;

        thread_pool(ull threadc = 0) {
            m_scheduledTasks.store(0);
            m_activeTasks.store(0);
            m_threadc = threadc == 0 ? std::thread::hardware_concurrency() : threadc;
            m_threadv.reset(new thread_data[m_threadc]);
            m_running = true;
            for (ull i = 0; i < m_threadc; ++i) {
                auto & x = m_threadv.get()[i];
                std::promise<void> p;
                auto f = p.get_future();
                x.thread = std::thread(threadFn, this, &x, &p);
                f.get();
            }
        }

        ~thread_pool() {
            {
                std::unique_lock<std::mutex> lck { m_mtx };
                m_running = false;
                m_cnd.notify_all();
            }
            for (ull i = 0, l = m_threadc; i < l; ++i) {
                auto & t = m_threadv.get()[i].thread;
                if (t.joinable()) {
                    try {
                        t.join();
                    } catch (std::system_error &) {} // join failed - mostly likely bc joinable state 
                }
            }
        }

        void enqueue(std::function<void(void)> fn) {
            if (!fn) { return; }
            {
                std::unique_lock<std::mutex> lck { m_mtx };
                m_tasks.emplace_back();
                m_tasks.back().fn = std::move(fn);
                ++m_scheduledTasks;
                m_cnd.notify_one();
            }
        }

        bool await(std::chrono::milliseconds timeout) const {
            auto cnd = [&] { return m_activeTasks == 0 && m_scheduledTasks == 0; };
            if (cnd()) { return true; }
            std::unique_lock<std::mutex> lckAwait { m_mtxAwait };
            m_cndAwait.wait_for(lckAwait, timeout);
            return cnd();
        }

        void await() const {
            auto cnd = [&] { return m_activeTasks == 0 && m_scheduledTasks == 0; };
            if (cnd()) { return; }
            std::unique_lock<std::mutex> lckAwait { m_mtxAwait };
            while (!cnd()) { m_cndAwait.wait(lckAwait); }
        }

        ull activeTasks() const noexcept { return m_activeTasks; }
        ull scheduledTasks() const noexcept { return m_scheduledTasks; }
        ull threadCount() const noexcept { return m_threadc; }

        static void threadFn(thread_pool * this_, thread_data * data, std::promise<void> * start_) noexcept {
            start_->set_value();
            {
                while (true) {
                    task_t task {};
                    {
                        std::unique_lock<std::mutex> lck { this_->m_mtx };
                        while (true) {
                            if (!this_->m_running) { return; }
                            auto & tasks = this_->m_tasks;
                            auto & taskIdx = this_->m_taskIdx;
                            if (!tasks.empty()) {
                                if (taskIdx >= this_->m_tasks.size()) { taskIdx = 0; } // reset task tracking index
                                task = std::move(tasks[taskIdx]); // get task at tracked index
                                if (tasks.size() > 1) { // if there were at least 2 tasks...
                                    tasks[taskIdx] = std::move(*(tasks.end() - 1)); // move the last added task to the current index
                                }
                                tasks.erase(tasks.end() - 1); // delete empty place (either we just retrieved from here or the task here was moved to the tracking index
                                ++taskIdx; // increment
                                if (taskIdx >= tasks.size()) { taskIdx = 0; } // reset index again if it was out of bounds - will preserve task order
                                ++this_->m_activeTasks;
                                --this_->m_scheduledTasks;
                                break;
                            }
                            this_->m_cnd.wait(lck);
                        }
                    }
                    try {
                        task.fn();
                    } catch (...) {
                        if (this_->exceptionHandler) {
                            //try {
                            this_->exceptionHandler(std::current_exception());
                            //} catch (...) { std::terminate(); }
                        }
                    }
                    --this_->m_activeTasks;
                    this_->m_cndAwait.notify_all();
                }
            }
        }

    };

}

#endif
