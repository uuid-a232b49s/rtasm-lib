#define _CRT_SECURE_NO_WARNINGS 1

#include <mutex>
#include <cstdio>
#include <string>
#include <fstream>
#include <iostream>

using ull = unsigned long long;

static std::mutex g_printMtx;
#define x_print(out, expr) \
do { \
    std::lock_guard<std::mutex> lck { g_printMtx }; \
    (out) << expr; \
} while(0)

#include "thread-pool.hxx"

static void scanFile(const std::string & file, ull fileIdx) {
    x_print(std::cerr, "scanning source: " << file << std::endl);
    std::ifstream in { file };
    char identifier[256];
    ull lineN = 0;
    std::string line;
    while (std::getline(in, line)) {
        auto r = std::sscanf(&line[0], " TEST %*[(] %255[0-9a-zA-Z] %*[)] ", identifier);
        if (r == 1) {
            x_print(std::cerr, "test found: " << identifier << std::endl);
            x_print(std::cout, identifier << ";");
        }
        ++lineN;
    }
    //std::cout << std::flush;
}

int main(int argc, char ** argv) {
    if (argc != 2) {
        std::cerr << "unexpected arguments" << std::endl;
        return -1;
    }
    rtasm::thread_pool threads;
    std::ifstream sources { argv[1] };
    std::string line;
    ull fileIdx = 0;
    while (std::getline(sources, line)) {
        threads.enqueue([=] () { scanFile(line, fileIdx); });
        ++fileIdx;
    }
}
