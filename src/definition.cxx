#include "rtasm/definition.hpp"

namespace rtasm {

    const ITypeDef::SInt ITypeDef::SInt::Instance {};
    const ITypeDef::UInt ITypeDef::UInt::Instance {};
    const ITypeDef::Float ITypeDef::Float::Instance {};
    const ITypeDef::Void ITypeDef::Void::Instance {};

    bool rtasm::operator==(const TypeDefMeta & a, const TypeDefMeta & b) noexcept {
        if (a.indirection != b.indirection || !a.type || !b.type) { return false; }
        auto id = a.type->id();
        if (id != b.type->id()) { return false; }
        if (id != ITypeDef::Extern::Id) { return true; }
        auto tia = static_cast<const ITypeDef::Extern *>(a.type)->typeinfo();
        auto tib = static_cast<const ITypeDef::Extern *>(a.type)->typeinfo();
        if (!tia || !tib) { return true; }
        return *tia == *tib;
    }

}
