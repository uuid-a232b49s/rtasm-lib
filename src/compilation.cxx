#include <new>
#include <tuple>
#include <memory>
#include <vector>
#include <cstring>
#include <utility>
#include <typeinfo>
#include <typeindex>
#include <algorithm>
#include <functional>
#include <unordered_map>

#ifndef NDEBUG
#include <iostream>
#define x_print(x) std::cerr << x << std::endl
#else
#define x_print(x)
#endif

#include "rtasm/definition.hpp"
#include "rtasm/compilation.hpp"
#include "exception.hxx"
#include "allocator.hxx"

#include "../src.internal/internal/memory.hxx"
#include "compilation.hxx"

namespace {

    using namespace rtasm;
    using namespace rtasm::internal;

    static bool eq(const details::typeMeta_t & a, const details::typeMeta_t & b) noexcept {
        // indirection & typeid will match for same type objects
        if (a.indirection != b.indirection || a.typeId != b.typeId) { return false; }
        // only attempt std::type_info check if typeid indicates extern type and both type objects provide std::type_info
        if (a.typeId == ITypeDef::Extern::Id && a.typeInfo && b.typeInfo && (*a.typeInfo) != (*b.typeInfo)) { return false; }
        return true;
    }

    static bool eq(const details::typeMeta_t & a, const TypeDefMeta & b) noexcept {
        if (b.type == nullptr) { return false; }
        if (a.indirection != b.indirection || a.typeId != b.type->id()) { return false; }
        if (a.typeId == ITypeDef::Extern::Id) {
            auto e = static_cast<const ITypeDef::Extern *>(b.type);
            auto ti = e->typeinfo();
            if (ti && a.typeInfo) { return (*ti) == (*a.typeInfo); }
        }
        return true;
    }

    struct rtMethodImpl0 final : compilation::rtMethod {
        void * m_pointer = nullptr;
        usize m_index = -1;

        virtual void * pointer() const noexcept override { return m_pointer; }

        virtual void * cast0(const details::typeMeta_t * sig, usize argc) const override {
            if (m_sig.size() != argc + 1) { goto lbl_no; }
            for (usize i = 0; i < argc + 1; ++i) {
                if (!eq(m_sig[i], sig[i])) { goto lbl_no; }
            }
            return m_pointer;
        lbl_no: throw std::bad_cast();
        }

    };

    struct rtAssemblyImpl final : Assembly {
        std::unique_ptr<void, memory::deleter_t> m_memory;
        std::vector<rtMethodImpl0> m_methods;
        virtual usize methodc() const noexcept override { return m_methods.size(); }
        virtual const Method * methodv(usize i) const override { return &m_methods.at(i); }
    };

    static details::typeMeta_t conv(const TypeDefMeta & m) {
        const std::type_info * ti = nullptr;
        RTASM_ASSERT_NON_NULL(m.type, type);
        if (m.type->id() == ITypeDef::Extern::Id) { ti = dynamic_cast<const ITypeDef::Extern &>(*m.type).typeinfo(); }
        return details::typeMeta_t { ti, m.indirection, m.type->id() };
    }

    template<typename element_t, typename container_t>
    static void addUnique(container_t & container, element_t value) {
        auto it = std::find(container.begin(), container.end(), value);
        if (it == container.end()) {
            container.emplace_back(std::move(value));
        }
    }

}

namespace {

    static usize cantorPairing(usize a, usize b) noexcept {
        return (a + b) * (a + b + 1) / 2 + b; // probably use something that handles overflows in a predictable way
    }

    template<typename value_t>
    static void hash1(usize & hash, const value_t & value) noexcept {
        auto h = std::hash<value_t>()(value);
        hash = cantorPairing(hash, h);
    }

    template<>
    inline void hash1(usize & hash, const details::typeMeta_t & a) noexcept {
        hash1(hash, a.indirection);
        hash1(hash, a.typeId);
        hash1(hash, std::type_index(*a.typeInfo));
    }

    template<>
    inline void hash1(usize & hash, const TypeDefMeta & a) noexcept {
        auto id = a.type->id();
        hash1(hash, a.indirection);
        if (id == ITypeDef::Extern::Id) {
            auto ti = static_cast<const ITypeDef::Extern *>(a.type)->typeinfo();
            if (ti) {
                hash1(hash, std::type_index(*ti));
            } else {
                struct a {};
                hash1(hash, std::type_index(typeid(a)));
            }
        } else hash1(hash, id);
    }

    template<>
    inline void hash1(usize & hash, const compilation::rtOpResult & a) noexcept {
        hash1(hash, a.locIdx);
        hash1(hash, a.type);
    }

    static void hash00(usize) noexcept {}
    template<typename arg_t, typename ... args_t>
    inline static void hash00(usize & hash, const arg_t & arg, const args_t & ... args) noexcept {
        hash1(hash, arg);
        hash00(hash, args...);
    }

    [[maybe_unused]] static usize hash0() noexcept { return 0; }
    template<typename ... args_t>
    inline static usize hash0(const args_t & ... args) noexcept {
        usize h = 0;
        hash00(h, args...);
        return h;
    }

}

#define rtasm_assert(condition, msg, opcodeIdx) ((void) ( (!!(condition)) ? 0 : (RTASM_THROW_CE(msg, opcodeIdx)) ))

namespace {

#define rtasm_cbAssignOpResult(rhs, lhs, verify, opIdx) \
do { \
    if (verify) { \
        rtasm_assert(((rhs).locIdx == (lhs).locIdx), "cross-branch divergence", opIdx); \
        rtasm_assert(((rhs).type == (lhs).type), "cross-branch divergence", opIdx); \
    } else (rhs) = (lhs); \
} while (0)

    static void analyzeMethod(rtMethodImpl0 * m, const IMethodDef * def, compilation::rtAsmData & output) {
        output.method = m;
        output.methodDef = def;

        using namespace compilation;
        usize opcodec = def->opcodec();

        std::vector<rtOp> & ops = output.ops;
        ops.resize(opcodec);
        std::vector<bool> opsInit; // tack ops which have already been visited
        opsInit.resize(opcodec);
        std::vector<usize> opsStateHash;
        opsStateHash.resize(opcodec);

        bool isVoidRet = m->m_sig[0].typeId == ITypeDef::Void::Id;

        // pre-record opcodes to lower access count to the method definition
        for (usize i = 0; i < opcodec; ++i) {
            auto & op = ops[i];
            op.op = def->opcodev(i);
            op.index = i;
        }

        std::unordered_map<usize, usize> lblToIdx;
        for (usize i = 0; i < opcodec; ++i) { // map out all labels
            auto & op = ops[i];
            if (op.op.m_code == OpCode::code_t::lbl) {
                rtasm_assert(lblToIdx.count(op.op.m_data.u) == 0, "duplicate label id", i);
                lblToIdx[op.op.m_data.u] = i;
            }
        }

        struct branch_t {
            usize opcodeIdx = 0;
            std::vector<rtOp *> stack;
            std::vector<bool> storage;
            rtAsmData * m_output = nullptr;

            usize useStorage() {
                usize l = storage.size();
                for (usize i = 0; i < l; ++i) {
                    if (!storage[i]) {
                        storage[i] = true;
                        return i;
                    } else continue;
                }
                storage.emplace_back(true);
                m_output->tmpStorageCount = std::max(m_output->tmpStorageCount, l + 1);
                return l;
            }

            void freeStorage(usize idx) {
                RTASM_ASSERT_RANGE_MAX(idx, storage.size(), storageIndex);
                storage[idx] = false;
            }

            usize stateHash() const noexcept {
                usize h = 0;
                for (auto & x : stack) { hash00(h, x->output); }
                // for (usize i = 0, l = storage.size(); i < l; ++i) { if (storage[i]) hash00(h, i); } // encoded above in x->output.locIdx
                return h;
            }

        };
        std::vector<branch_t> branches;
        branches.emplace_back();

        while (!branches.empty()) {
            auto b = [&] () noexcept -> branch_t & { return branches.front(); };
            b().m_output = &output;

            while (true) {
                auto opcodeIdx = b().opcodeIdx;
                rtasm_assert(opcodeIdx < opcodec, "opcode out of bounds", b().opcodeIdx);

                auto & op = ops[opcodeIdx];
                bool crossBranchVerify = false; // when true: exit when verification is complete
                if (!opsInit[opcodeIdx]) {
                    opsInit[opcodeIdx] = true;
                    opsStateHash[opcodeIdx] = b().stateHash();
                } else {
                    // - should be here bc of an iterative jump - jumped here from elsewhere
                    // - check branch states for this instruction & exit
                    // - no need to check the rest of the execution path - if the beginning matches
                    //   across branches then the path will identical from this instruction forward
                    crossBranchVerify = true;
                    rtasm_assert(opsStateHash[opcodeIdx] == b().stateHash(), "cross-branch state mismatch", opcodeIdx);
                }

                // puts currently processed opcode onto the stack
                auto putStackValue = [&] () {
                    b().stack.emplace_back(&op);
                    op.output.locIdx = b().useStorage();
                };

                // retrieves top-most stack value
                auto getStackValue = [&] () -> rtOp * {
                    auto & stack = b().stack;
                    rtasm_assert(!stack.empty(), "stack empty", opcodeIdx);
                    auto op_ = stack.back();
                    stack.pop_back();
                    b().freeStorage(op_->output.locIdx);
                    return op_;
                };

                switch (op.op.m_code) { // process current opcode
                    case OpCode::code_t::arg:
                        rtasm_assert((op.op.m_data.u + 1) < m->m_sig.size(), "illegal argument index", opcodeIdx);
                        op.output.type = def->argv(op.op.m_data.u);
                        putStackValue();
                        ++opcodeIdx;
                        break;

                    case OpCode::code_t::add:
                    case OpCode::code_t::sub: {
                        auto a1 = getStackValue();
                        auto a0 = getStackValue();
                        rtasm_assert(a0->output.type == a1->output.type, "mismatching operand types", opcodeIdx);
                        op.inputs.resize(2);
                        rtasm_cbAssignOpResult(op.inputs[0].input, a0->output, crossBranchVerify, opcodeIdx);
                        rtasm_cbAssignOpResult(op.inputs[1].input, a1->output, crossBranchVerify, opcodeIdx);
                        addUnique(op.inputs[0].sources, a0);
                        addUnique(op.inputs[1].sources, a1);
                        op.output.type = a0->output.type;
                        putStackValue();
                        ++opcodeIdx;
                        break;
                    }

                    case OpCode::code_t::ret:
                        if (!isVoidRet) {
                            auto a0 = getStackValue();
                            rtasm_assert(eq(m->m_sig[0], a0->output.type), "mismatching return type", opcodeIdx);
                            op.inputs.resize(1);
                            rtasm_cbAssignOpResult(op.inputs[0].input, a0->output, crossBranchVerify, opcodeIdx);
                            addUnique(op.inputs[0].sources, a0);
                        }
                        rtasm_assert(b().stack.empty(), "stack not empty upon return", opcodeIdx);
                        opcodeIdx = invalVal;
                        break;

                    case OpCode::code_t::jmp: {
                        auto it = lblToIdx.find(op.op.m_data.u);
                        rtasm_assert(it != lblToIdx.end(), "label not found", opcodeIdx);
                        opcodeIdx = it->second;
                        break;
                    }

                    case OpCode::code_t::jmp_cond: {
                        auto a0 = getStackValue();
                        op.inputs.resize(1);
                        rtasm_cbAssignOpResult(op.inputs[0].input, a0->output, crossBranchVerify, opcodeIdx);
                        addUnique(op.inputs[0].sources, a0);
                        if (!crossBranchVerify) { // divergence - create new branch if not verifying
                            auto it = lblToIdx.find(op.op.m_data.uu.a);
                            rtasm_assert(it != lblToIdx.end(), "label not found", opcodeIdx);
                            branches.emplace_back();
                            branches.back().opcodeIdx = it->second;
                        }
                        ++opcodeIdx;
                        break;
                    }

                    case OpCode::code_t::lbl: // was marked earlier
                    case OpCode::code_t::null: // nop
                        break;
                    default: RTASM_THROW_CE("unknown opcode", opcodeIdx);
                }

                if (crossBranchVerify) { break; }
                if (op.op.m_code == OpCode::code_t::ret) { break; }
                b().opcodeIdx = opcodeIdx;
            }

            branches.erase(branches.begin());
        }

        for (auto & op : ops) {
            std::cerr << op.op.m_name << " " << op.output.locIdx;
            for (auto & i : op.inputs) { std::cerr << " " << i.input.locIdx; }
            std::cerr << std::endl;
        }

        // recalc jmp offsets before doing this
        /*for (usize i = ops.size() - 1; i > 0; --i) {
            if (ops[i].op.m_code == OpCode::code_t::null) {
                ops.erase(ops.begin() + i);
            }
        }*/
    }

}

bool rtasm::internal::compilation::isVoid(const TypeDefMeta & m) noexcept {
    return m.indirection == 0 && m.type != nullptr && m.type->id() == ITypeDef::Void::Id;
}

namespace rtasm {

    std::unique_ptr<const Assembly> compile(const IAssemblyDef & def) {
        //internal::CachedAllocator allocator;
        std::unique_ptr<rtAssemblyImpl> a { new rtAssemblyImpl() };
        a->userData = def.userData();

        // allocate & 'declare' methods
        auto methodc = def.methodc();
        a->m_methods.resize(methodc);
        for (usize i = 0; i < methodc; ++i) {
            auto md = def.methodv(i); RTASM_ASSERT_NON_NULL(md, IMethodDef);
            auto & mi = a->m_methods[i];
            mi.userData = md->userData();
            auto argc = md->argc();
            mi.m_sig.reserve(argc);
            for (usize i = 0; i < argc; ++i) { mi.m_sig.emplace_back(conv(md->argv(i))); }
            mi.m_sig.emplace_back(conv(md->argv(i)));
            mi.m_index = i;
        }

        // analyze & compile methods
        std::vector<compilation::rtCompilationData> cmpData;
        cmpData.resize(methodc);
        for (usize i = 0; i < methodc; ++i) {
            auto md = def.methodv(i); RTASM_ASSERT_NON_NULL(md, IMethodDef);
            auto mc = &a->m_methods[i];
            compilation::rtAsmData asmData;
            analyzeMethod(mc, md, asmData);
            compilation::compileMethod(asmData, cmpData[i]);
        }

        // write machine code to executable memory
        usize asmSize = 0;
        for (auto & c : cmpData) { asmSize += c.data.size(); }
        a->m_memory.reset(memory::allocate(asmSize));
        if (!a->m_memory) { RTASM_THROW_CE("failed to allocate executable memory", -1); }

        for (usize i = 0, asmOffset = 0; i < methodc; ++i) {
            auto s = cmpData[i].data.size();
            void * m = reinterpret_cast<unsigned char *>(a->m_memory.get()) + asmOffset;
            std::memcpy(m, &cmpData[i].data[0], s);
            a->m_methods[i].m_pointer = m;
            asmOffset += s;
        }

        if (!memory::protect(a->m_memory.get())) { RTASM_THROW_CE("failed to mark memory as executable", -1); }

        return a;
    }

}
