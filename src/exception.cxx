#include "exception.hxx"

const char * rtNullPtrException::what() const noexcept { return rtExMsg::msg(); }
const char * rtNullPtrException::file() const noexcept { return rtExData::m_file; }
rtasm::usize rtNullPtrException::line() const noexcept { return rtExData::m_line; }

const char * rtCompilationException::what() const noexcept { return rtExMsg::msg(); }
const char * rtCompilationException::file() const noexcept { return rtExData::m_file; }
rtasm::usize rtCompilationException::line() const noexcept { return rtExData::m_line; }
rtasm::isize rtCompilationException::opcodeIdx() const noexcept { return m_opcodeIdx; }

const char * rt_range_error::what() const noexcept { return rtExMsg::msg(); }
