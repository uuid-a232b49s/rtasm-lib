#ifndef HEADER_RTASM_SRC_COMPILATION
#define HEADER_RTASM_SRC_COMPILATION 1

#include <vector>
#include <cstdint>

#include "../include/rtasm/common.hpp"
#include "../include/rtasm/compilation.hpp"

namespace rtasm {
    namespace internal {
        namespace compilation {

            struct rtOp;

            struct rtMethod : Method {
                std::vector<details::typeMeta_t> m_sig; // [ ret, args... ]
            };

            constexpr usize invalVal = ~0ull;

            struct rtOpResult {
                TypeDefMeta type; // result type
                usize locIdx = invalVal;
            };

            struct rtOpInput {
                rtOpResult input;
                std::vector<rtOp *> sources;
            };

            struct rtOp {
                usize index = invalVal; // ordinal index of this opcode
                OpCode op;
                std::vector<rtOpInput> inputs; // data inputs for this opcode
                rtOpResult output;
            };

            struct rtAsmData {
                const rtMethod * method = nullptr;
                const IMethodDef * methodDef = nullptr;
                std::vector<rtOp> ops;
                usize tmpStorageCount = 0;
            };

            struct rtCompilationData {
                std::vector<std::uint8_t> data;
            };

            bool isVoid(const TypeDefMeta &) noexcept;

            extern void compileMethod(rtAsmData &, rtCompilationData & output);

        }
    }
}

#endif
