#include <cstdlib>
//#include <algorithm>

#include "allocator.hxx"

namespace rtasm {
    namespace internal {

        namespace {
            struct alignas(std::max_align_t) header_t { usize size; };
        }

        void CachedAllocator::free_deleter_t::operator()(void * m) const noexcept { std::free(reinterpret_cast<unsigned char *>(m) - sizeof(header_t)); }

        CachedAllocator::~CachedAllocator() = default;

        void * CachedAllocator::allocate(usize size) {
            // this will end up with more writes
            /*auto e = std::upper_bound(m_memory.begin(), m_memory.end(), size, [] (usize s, const element_t & e) { return s < e.size; });
            if (e != m_memory.end()) {
                if (e->size <= size * 2) {

                }
            }*/
            for (auto & x : m_memory) {
                if (x.memory) {
                    if (x.size == size || (x.size > size && x.size <= size * 2)) { return x.memory.release(); }
                }
            }
            auto m = std::malloc(sizeof(header_t) + size);
            if (!m) { throw std::bad_alloc(); }
            reinterpret_cast<header_t *>(m)->size = size;
            return reinterpret_cast<unsigned char *>(m) + sizeof(header_t);
        }

        void CachedAllocator::deallocate(void * m) {
            auto size = reinterpret_cast<header_t *>(reinterpret_cast<unsigned char *>(m) - sizeof(header_t))->size;
            for (auto & x : m_memory) {
                if (!x.memory) {
                    x.size = size;
                    x.memory.reset(m);
                    return;
                }
            }
            m_memory.emplace_back(element_t { decltype(element_t::memory)(m), size });
        }

    }
}
