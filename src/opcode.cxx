#include "rtasm/opcode.hpp"

namespace rtasm {

    OpCode OpCode::arg(usize argIdx) noexcept {
        OpCode c;
        c.m_name = "arg";
        c.m_code = code_t::arg;
        c.m_data.u = argIdx;
        return c;
    }

    OpCode OpCode::add() noexcept {
        OpCode c;
        c.m_name = "add";
        c.m_code = code_t::add;
        return c;
    }

    OpCode OpCode::ret() noexcept {
        OpCode c;
        c.m_name = "ret";
        c.m_code = code_t::ret;
        return c;
    }

    OpCode OpCode::jmp(usize lblId) noexcept {
        OpCode c;
        c.m_name = "jmp";
        c.m_code = code_t::jmp;
        c.m_data.u = lblId;
        return c;
    }

    OpCode OpCode::br(bool onTrue, usize lblId) noexcept {
        OpCode c;
        c.m_name = "br";
        c.m_code = code_t::jmp_cond;
        c.m_data.uu.a = lblId;
        c.m_data.uu.b = onTrue ? 1 : 2;
        return c;
    }

    OpCode OpCode::label(usize lblId) noexcept {
        OpCode c;
        c.m_name = "lbl";
        c.m_code = code_t::lbl;
        c.m_data.u = lblId;
        return c;
    }

}
