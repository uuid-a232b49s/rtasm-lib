#ifndef HEADER_RTASM_SRC_EXCEPTION
#define HEADER_RTASM_SRC_EXCEPTION 1

#include <string>
#include <utility>
#include <stdexcept>

#include "rtasm/common.hpp"
#include "rtasm/exception.hpp"

struct rtExData {
    const char * m_file;
    rtasm::usize m_line;
};

struct rtExMsg {
    const char * m_cstr;
    std::string m_str;
    rtExMsg(std::string msg) noexcept : m_cstr(nullptr), m_str(std::move(msg)) {}
    rtExMsg(const char * msg) noexcept : m_cstr(msg) {}
    const char * msg() const noexcept { return m_cstr ? m_cstr : m_str.c_str(); }
};

class rtNullPtrException : public rtasm::NullPtrException, rtExMsg, rtExData {
public:
    rtNullPtrException(rtExMsg msg, rtExData data) : rtExMsg(std::move(msg)), rtExData(std::move(data)) {}
    virtual const char * what() const noexcept override;
    virtual const char * file() const noexcept override;
    virtual rtasm::usize line() const noexcept override;
};
#define RTASM_THROW_NP(msg) throw rtNullPtrException(rtExMsg(msg), rtExData { __FILE__, __LINE__ })
#define RTASM_ASSERT_NON_NULL(var, displayName) ((void)( ((var) == nullptr) ? RTASM_THROW_NP("<" #displayName "> is null") : 0 ))

class rtCompilationException : public rtasm::CompilationException, rtExMsg, rtExData {
public:
    rtasm::isize m_opcodeIdx;
    rtCompilationException(decltype(m_opcodeIdx) opcodeIdx, rtExMsg msg, rtExData data)
        : rtExMsg(std::move(msg)), rtExData(std::move(data)), m_opcodeIdx(opcodeIdx) {}
    virtual const char * what() const noexcept override;
    virtual const char * file() const noexcept override;
    virtual rtasm::usize line() const noexcept override;
    virtual rtasm::isize opcodeIdx() const noexcept override;
};
#define RTASM_THROW_CE(msg, opcodeIdx) throw rtCompilationException((decltype(rtCompilationException::m_opcodeIdx))(opcodeIdx), rtExMsg(msg), rtExData { __FILE__, __LINE__ })

class rt_range_error : public std::range_error, rtExMsg {
public:
    rt_range_error(rtExMsg msg) : std::range_error(""), rtExMsg(std::move(msg)) {}
    virtual const char * what() const noexcept override;
};
#define RTASM_THROW_RANGE(msg) throw rt_range_error(msg)
#define RTASM_ASSERT_RANGE_MAX(val, max, displayName) ((void)( (val) > (max) ? RTASM_THROW_RANGE("value <" #displayName "> exceeds maximal constraint") : 0 ))
#define RTASM_ASSERT_RANGE_MIN(val, min, displayName) ((void)( (val) < (min) ? RTASM_THROW_RANGE("value <" #displayName "> exceeds minimal constraint") : 0 ))
#define RTASM_ASSERT_RANGE(val, min, max, displayName) ((void) ( RTASM_ASSERT_RANGE_MIN(val, min, displayName), RTASM_ASSERT_RANGE_MAX(val, max, displayName) ))

#endif
