#ifndef HEADER_RTASM_SRC_ALLOCATOR
#define HEADER_RTASM_SRC_ALLOCATOR 1

#include <vector>
#include <memory>
#include <cstddef>

#include "../include/rtasm/common.hpp"

namespace rtasm {
    namespace internal {

        struct CachedAllocator {

            struct free_deleter_t { void operator()(void *) const noexcept; };

            struct element_t {
                std::unique_ptr<void, free_deleter_t> memory;
                usize size;
            };

            ~CachedAllocator();

            std::vector<element_t> m_memory;

            void * allocate(usize size);

            void deallocate(void * m);

        };

        template<typename x, typename srcAlloc = CachedAllocator>
        struct AllocatorAdapter {
            using value_type = x;

            srcAlloc * m_allocator;

            AllocatorAdapter(srcAlloc * allocator) : m_allocator(allocator) {}

            x * allocate(usize n) { return static_cast<x *>(m_allocator->allocate(sizeof(x) * n)); }

            void deallocate(x * p, usize n) { m_allocator->deallocate(p); }

            friend bool operator==(const AllocatorAdapter & a, const AllocatorAdapter & b) noexcept { return a.m_allocator == b.m_allocator; }

        };

    }
}

#endif
