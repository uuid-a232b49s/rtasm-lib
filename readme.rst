rtasm-lib
=========


What is this?
-------------

- A runtime compiler

- Takes a set of high-level opcodes & compiles them to native instructions

- Under development


Supported Environments (planned)
--------------------------------

- x64

  - Clang, GCC, MSVC


License
-------

Boost Software License - Version 1.0


Building
--------

- Dependencies

  - ``cmake`` 3.14+

  - ``c++17`` compiler

    - intended to be c++11 in future

1. Create build dir

   .. code-block:: bash

     mkdir _build
     cd _build

2. Initialize cmake cache

   .. code-block:: bash

     cmake -G Ninja ..

   - replace/remove ``-G Ninja`` as needed

3. Run build

   .. code-block:: bash

     cmake --build .

4. Run tests (probably a good practice)

   .. code-block:: bash

     ctest


CMake
-----

- static library target: ``rt-asm``

- tests variable: ``RTASMLIB_TESTS``

  - ``ON`` by default

- linking

  .. code-block:: cmake

    # define executable/library
    add_executable(SomeObject ...)
    #add_library(SomeObject ...)
    ...
    target_link_libraries(SomeObject rt-asm) # link lib


Why only specific environments are supported?
---------------------------------------------

- This library has a limit as to which compilers & architectures it can work with

- Why is that?

  - Each compiler is free to use its own implementation of certain mechanisms (i.e. calling conventions)

  - This means that this library must know how to interact with the envorionment that a specific compiler implements

  - On top of that each cpu architecture is likely to have its own set of instructions, meaning that a different assembly will be emitted for a different cpu arch


Gameplan
--------

- [ ] More opcodes

  - [ ] arithmetic

    - [x] add

  - [ ] jump & branch

  - [ ] dereference

  - [ ] call

  - [ ] (?) atomics

- [ ] More tests

- [ ] Test & refine api

  - add C-compatible version
