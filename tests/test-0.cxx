#include <cassert>
#include <memory>
#include <cstring>

#include "../src.internal/internal/memory.hxx"

template<typename emitMethod_t>
static auto makeMethod(unsigned long long size, emitMethod_t emitMethod) -> std::unique_ptr<void, rtasm::internal::memory::deleter_t> {
    using namespace rtasm::internal;
    std::unique_ptr<void, memory::deleter_t> m { memory::allocate(size), memory::deleter_t() };
    if (!m) { return {}; }
    emitMethod(static_cast<unsigned char *>(m.get()));
    if (!memory::protect(m.get())) { return {}; }
    return m;
}

int main() {
    using namespace rtasm::internal;

    {
        auto m = makeMethod(256, [] (unsigned char * d) {
            d[0] = 0xc3;
        });
        if (!m) { return 1; }
        auto method = reinterpret_cast<void(*)(void)>(m.get());
        method();
    }

    {
        auto m = makeMethod(256, [] (unsigned char * d) {
            unsigned char data[] = {
                0xb8, 0x49, 0xad, 0x45, 0x00,
                0xc3
            };
            std::memcpy(d, data, sizeof(data));
        });
        if (!m) { return 1; }
        auto method = reinterpret_cast<unsigned long long(*)(void)>(m.get());
        auto v = method();
        assert(v == 0x45ad49);
    }

    {
        auto m = makeMethod(256, [] (unsigned char * d) {
            unsigned char data[] = {
                0x48, 0xb8, 0xff, 0xf1, 0x71, 0xdc, 0xa6, 0x3a, 0xa2, 0x00,
                0xc3
            };
            std::memcpy(d, data, sizeof(data));
        });
        if (!m) { return 1; }
        auto method = reinterpret_cast<unsigned long long(*)(void)>(m.get());
        auto v = method();
        assert(v == 0xa23aa6dc71f1ff);
    }

    {
        auto m = makeMethod(256, [] (unsigned char * d) {
            unsigned char data[] = {
                0x55,
                0x48, 0x89, 0xe5,
                0x48, 0xb8, 0xff, 0xf1, 0x71, 0xdc, 0xa6, 0x3a, 0xa2, 0x00,
                0x5d,
                0xc3
            };
            std::memcpy(d, data, sizeof(data));
        });
        if (!m) { return 1; }
        auto method = reinterpret_cast<unsigned long long(*)(void)>(m.get());
        auto v = method();
        assert(v == 0xa23aa6dc71f1ff);
    }

    return 0;
}
