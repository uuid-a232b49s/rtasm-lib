cmake_minimum_required(VERSION 3.14)

rtasm_platform_sources("${CMAKE_CURRENT_LIST_DIR}" FALSE "*.cxx" _p_paths)
file(GLOB _paths "${CMAKE_CURRENT_LIST_DIR}/*.cxx")

foreach(_path IN LISTS _paths _p_paths)
    file(RELATIVE_PATH _name "${CMAKE_CURRENT_LIST_DIR}" "${_path}")
    string(REGEX REPLACE "\\.[^.]*$" "" _name ${_name}) # remove extension from path
    string(MAKE_C_IDENTIFIER "rtasm-${_name}" _tname)
    add_executable(${_tname} "${_path}")
    target_link_libraries(${_tname} rtasm)
    add_test(NAME ${_tname} COMMAND ${_tname})
endforeach()
