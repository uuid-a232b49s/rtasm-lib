#include <cassert>
#include <utility>

#include "rtasm/definition.hpp"
#include "rtasm/compilation.hpp"

#include "rtasm/definition.basic.hpp"

int main() {
    using namespace rtasm;

    BasicAssemblyDef a;

    {
        std::unique_ptr<BasicMethodDef> m { new BasicMethodDef() };
        m->m_args = { types::UInt::Instance, types::UInt::Instance };
        m->m_ret = types::UInt::Instance;
        m->m_opcodes = {
            OpCode::arg(0),
            OpCode::arg(1),
            OpCode::add(),
            OpCode::arg(0),
            OpCode::add(),
            OpCode::ret()
        };
        a.m_methods.emplace_back(std::move(m));
    }

    auto ca = compile(a);

    auto m = ca->methodv(0)->cast<ntypes::UInt(ntypes::UInt, ntypes::UInt)>();
    auto ret = m(5, 6);
    assert(ret == (5 + 6 + 5));

    return 0;
}
