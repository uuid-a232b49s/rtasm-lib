#include <cassert>

#include "../../src.internal/x64/register.hxx"

int main() {
    using namespace rtasm::internal;
    assert(x64::ModRM().reg(2) == 0x10);
    assert(x64::ModRM().mod(1, 1).reg(2) == 0xd0);
}
