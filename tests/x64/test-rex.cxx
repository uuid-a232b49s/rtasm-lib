#include <cassert>

#include "../../src.internal/x64/register.hxx"

int main() {
    using namespace rtasm::internal;
    assert(x64::REX().w(1) == 0x48);
}
