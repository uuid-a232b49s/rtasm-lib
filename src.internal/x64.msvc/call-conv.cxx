#include "../../src/exception.hxx"

#include "../x64/common.hxx"
#include "../x64/register.hxx"
#include "../internal/call-conv.hxx"

namespace {
    using namespace rtasm::internal::call_conv;

    static RegInfo RegInfo0(bool varSafe, bool calleePreserved) noexcept {
        RegInfo a;
        a.calleePreserved = calleePreserved;
        a.varSafe = varSafe;
        return a;
    }

}

// msvc x64
// https://docs.microsoft.com/en-us/cpp/build/x64-calling-convention?view=msvc-170

namespace rtasm {
    namespace internal {
        namespace call_conv {

            const usize stackAlign = 16;

            RegInfo regInfo(usize registerId) {
                switch (static_cast<x64::REG>(registerId)) {
                    case x64::REG::RAX: 
                    case x64::REG::RCX:
                    case x64::REG::RDX:
                    case x64::REG::R8:
                    case x64::REG::R9:
                    case x64::REG::R10:
                    case x64::REG::R11: return RegInfo0(true, false);
                    case x64::REG::RSP:
                    case x64::REG::RBP: return RegInfo0(false, true);
                    case x64::REG::RBX:
                    case x64::REG::RSI:
                    case x64::REG::RDI:
                    case x64::REG::R12:
                    case x64::REG::R13:
                    case x64::REG::R14:
                    case x64::REG::R15: return RegInfo0(true, true);
                    case x64::REG::XMM0:
                    case x64::REG::XMM1:
                    case x64::REG::XMM2:
                    case x64::REG::XMM3:
                    case x64::REG::XMM4:
                    case x64::REG::XMM5: return RegInfo0(true, false);
                    case x64::REG::XMM7:
                    case x64::REG::XMM8:
                    case x64::REG::XMM9:
                    case x64::REG::XMM10:
                    case x64::REG::XMM11:
                    case x64::REG::XMM12:
                    case x64::REG::XMM13:
                    case x64::REG::XMM14:
                    case x64::REG::XMM15: return RegInfo0(true, true);
                    default: RTASM_THROW_CE("illegal register id", -1);
                }
            }

            void getArgLayout(const compilation::rtAsmData * data, StorageLoc * retLoc, StorageLoc * argLocs) {
                {
                    auto ret = data->methodDef->ret();
                    switch (x64::storageType(ret)) {
                        case x64::StorageType::integral:
                            retLoc->loc = StorageLoc::loc_t::reg;
                            retLoc->reg = static_cast<usize>(x64::REG::RAX);
                            break;
                        case x64::StorageType::floating:
                            retLoc->loc = StorageLoc::loc_t::reg;
                            retLoc->reg = static_cast<usize>(x64::REG::XMM0);
                            break;
                        default:
                            if (compilation::isVoid(ret)) {
                                retLoc->loc = StorageLoc::loc_t::null;
                            } else RTASM_THROW_CE("unsupported return type", -1);
                    }
                }
                {
                    auto argc = data->method->m_sig.size() - 1;
                    usize nextArgStackIdx = 0;
                    for (usize i = 0; i < argc; ++i) {
                        auto & argLoc = argLocs[i];
                        switch (x64::storageType(data->methodDef->argv(i))) {
                            case x64::StorageType::integral:
                                if (i < 4) {
                                    static x64::REG arg[] { x64::REG::RCX, x64::REG::RDX, x64::REG::R8, x64::REG::R9 };
                                    argLoc.loc = StorageLoc::loc_t::reg;
                                    argLoc.reg = static_cast<usize>(arg[i]);
                                } else {
                                    argLoc.loc = StorageLoc::loc_t::stack;
                                    argLoc.stackIdx = nextArgStackIdx++;
                                }
                                break;
                            case x64::StorageType::floating:
                                if (i < 4) {
                                    static x64::REG arg[] { x64::REG::XMM0, x64::REG::XMM1, x64::REG::XMM2, x64::REG::XMM3 };
                                    argLoc.loc = StorageLoc::loc_t::reg;
                                    argLoc.reg = static_cast<usize>(arg[i]);
                                } else {
                                    argLoc.loc = StorageLoc::loc_t::stack;
                                    argLoc.stackIdx = nextArgStackIdx++;
                                }
                                break;
                            default: RTASM_THROW_CE("unsupported arg type", -1);
                        }
                    }
                }
            }

        }
    }
}
