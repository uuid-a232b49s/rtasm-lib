#include <array>
#include <vector>
#include <utility>
#include <algorithm>
#include <initializer_list>

#include "../../src/exception.hxx"
#include "../../src/compilation.hxx"

#include "common.hxx"
#include "register.hxx"
#include "../internal/call-conv.hxx"

namespace rtasm {
    namespace internal {
        namespace compilation {

            namespace {

                template<typename container_t>
                static void ensureSize(container_t & c, usize size) {
                    if (size >= c.size()) {
                        c.resize(size);
                    }
                }

                struct StorageMgr {
                private:
                    std::vector<StorageLoc> m_tmpStorageLocs[2]; // currently mapped storage locations
                    std::vector<x64::REG> m_usableGPRs[2]; // cached usable storage GP regs

                    struct stackStorageLoc {
                        usize tmpId = invalVal;
                        usize size = 0, align = 0;
                    };
                    std::vector<stackStorageLoc> m_stackStorage; // anything stack related is untested - likely to blow up real bad

                    bool isUsedAsArg(x64::REG r) const {
                        for (auto & x : argLocs) {
                            if (x.loc == StorageLoc::loc_t::reg) {
                                if (x.reg == static_cast<usize>(r)) { return true; }
                            }
                        }
                        return false;
                    }

                public:

                    std::vector<StorageLoc> argLocs;
                    StorageLoc retLoc;
                    bool readOnly = false;

                    StorageMgr(rtAsmData * data) {
                        argLocs.resize(data->method->m_sig.size() - 1);
                        call_conv::getArgLayout(data, &retLoc, &argLocs[0]);
                    }

                    StorageLoc getTmpStorage(const TypeDefMeta & t, usize tmpId) {
                        std::vector<StorageLoc> * tsl;
                        std::vector<x64::REG> * tsg;
                        usize gpBegin, gpEnd;
                        auto xt = x64::storageType(t);
                        switch (xt) {
                            case x64::StorageType::integral:
                                tsl = &m_tmpStorageLocs[0];
                                tsg = &m_usableGPRs[0];
                                gpBegin = static_cast<usize>(x64::REG::gpBegin);
                                gpEnd = static_cast<usize>(x64::REG::gpEnd);
                                break;
                            case x64::StorageType::floating:
                                tsl = &m_tmpStorageLocs[1];
                                tsg = &m_usableGPRs[1];
                                gpBegin = static_cast<usize>(x64::REG::xmmBegin);
                                gpEnd = static_cast<usize>(x64::REG::xmmEnd);
                                break;
                            case x64::StorageType::stack: {
                                stackStorageLoc * s;
                                usize i = 0;
                                for (usize l = m_stackStorage.size(); i < l;) {
                                    if (m_stackStorage[i].tmpId == tmpId) {
                                        s = &m_stackStorage[i];
                                        goto lbl_a;
                                    }
                                    ++i;
                                }
                                if (readOnly) { RTASM_THROW_CE("unexpected compiler state", -1); }
                                m_stackStorage.emplace_back();
                                s = &m_stackStorage.back();
                                s->tmpId = tmpId;
                            lbl_a:;
                                if ((s->align = std::max(s->align, t.type->align())) == 0) { RTASM_THROW_CE("unexpected type align", -1); }
                                if ((s->size = std::max(s->size, t.type->size())) == 0) { RTASM_THROW_CE("unexpected type size", -1); }
                                StorageLoc sl;
                                sl.loc = StorageLoc::loc_t::stack;
                                sl.stackIdx = i;
                                return sl;
                            }
                            default: RTASM_THROW_CE("unsupported value type", -1);
                        }
                        {
                            if (tmpId < tsl->size() && (*tsl)[tmpId].loc != StorageLoc::loc_t::null) { // tmpId mapped?
                                return (*tsl)[tmpId]; // it is, return it
                            }
                            if (tsg->empty()) { // usable storage GPRs cached?
                                if (readOnly) { RTASM_THROW_CE("unexpected compiler state", -1); }
                                struct regInfo {
                                    x64::REG reg;
                                    call_conv::RegInfo info;
                                };
                                std::vector<regInfo> regs;
                                for (usize i = gpBegin; i < gpEnd; ++i) {
                                    if (isUsedAsArg(static_cast<x64::REG>(i))) { continue; }
                                    auto ri = call_conv::regInfo(i);
                                    if (!ri.varSafe) { continue; }
                                    regs.emplace_back();
                                    auto & ri0 = regs.back();
                                    ri0.reg = static_cast<x64::REG>(i);
                                    ri0.info = ri;
                                }
                                // assuming here there must be at least 1... should be the case on x64
                                if (regs.empty()) { RTASM_THROW_CE("failed to find any suitable tmp registers", -1); } // shouldnt happen, we need at least some bc most instructions accept at least 1 register for data manipulation
                                std::sort(regs.begin(), regs.end(), [] (const regInfo & a, const regInfo & b) -> bool {
                                    if (a.info.calleePreserved != b.info.calleePreserved) {
                                        return (a.info.calleePreserved ? 1 : 0) < (b.info.calleePreserved ? 1 : 0);
                                    }
                                    return a.reg < b.reg;
                                });
                                tsg->resize(regs.size());
                                for (usize i = 0, l = regs.size(); i < l; ++i) { (*tsg)[i] = regs[i].reg; }
                            }
                            //if (tmpId <= tsl->size()) { tsl->resize(tmpId + 1); }
                            ensureSize(*tsl, tmpId + 1);
                            for (auto r : (*tsg)) {
                                for (auto & x : (*tsl)) {
                                    if (x.loc == StorageLoc::loc_t::reg && x.reg == static_cast<usize>(r)) { // register already mapped
                                        goto lbl_continue;
                                    }
                                }
                                {
                                    auto & ret = (*tsl)[tmpId];
                                    ret.loc = StorageLoc::loc_t::reg;
                                    ret.reg = static_cast<usize>(r);
                                    return ret;
                                }
                            lbl_continue:;
                            }
                            { // no available registers
                                if (readOnly) { RTASM_THROW_CE("unexpected compiler state", -1); }
                                auto & ret = (*tsl)[tmpId];
                                ret.loc = StorageLoc::loc_t::stack;
                                ret.stackIdx = m_stackStorage.size();
                                m_stackStorage.emplace_back();
                                const ITypeDef * t;
                                switch (xt) {
                                    case x64::StorageType::integral: t = &ITypeDef::UInt::Instance; break;
                                    case x64::StorageType::floating: t = &ITypeDef::Float::Instance; break;
                                    default: RTASM_THROW_CE("unexpected compiler state", -1);
                                }
                                auto & ss = m_stackStorage.back();
                                ss.align = t->align();
                                ss.size = t->size();
                                return ret;
                            }
                        }
                    }

                    StorageLoc getTmpStorage(const compilation::rtOpResult & result) {
                        return getTmpStorage(result.type, result.locIdx);
                    }

                    StorageLoc getPersistentStorage(const TypeDefMeta & t, usize id) {
                        RTASM_THROW_CE("not implemented", -1);
                    }

                    std::vector<usize> stackIdx2Offset() const {
                        std::vector<usize> stackIdx2Offset;
                        stackIdx2Offset.resize(m_stackStorage.size());
                        for (usize i = 0, l = stackIdx2Offset.size(); i < l; ++i) { stackIdx2Offset[i] = i; }
                        std::sort(stackIdx2Offset.begin(), stackIdx2Offset.end(), [&] (usize a, usize b) { return m_stackStorage[a].align < m_stackStorage[b].align; });
                        usize lastOffset = 0;
                        for (auto & x : stackIdx2Offset) {
                            auto & ss = m_stackStorage[x];
                            auto pad = x64::alignPad(ss.align, lastOffset);
                            x = lastOffset + pad;
                            lastOffset += ss.size;
                        }
                        return stackIdx2Offset;
                    }

                };

            }

            namespace {

                static void write(rtCompilationData & output, std::initializer_list<std::uint8_t> data) {
                    output.data.insert(output.data.end(), data.begin(), data.end());
                }
                template<std::size_t L>
                static void write(rtCompilationData & output, const std::array<std::uint8_t, L> & data) {
                    output.data.insert(output.data.end(), data.begin(), data.end());
                }

                static void emitMOV(rtCompilationData & output, const StorageLoc & dst, const StorageLoc & src) {
                    if (dst == src) { return; }
                    if (dst.loc == StorageLoc::loc_t::reg && src.loc == StorageLoc::loc_t::reg) {
                        auto rDst = static_cast<x64::REG>(dst.reg);
                        auto rSrc = static_cast<x64::REG>(src.reg);
                        if (x64::isGp(rDst) && x64::isGp(rSrc)) {
                            write(output, {
                                x64::REX().w(1).r(x64::isGpRx(rSrc)).b(x64::isGpRx(rDst)),
                                0x89,
                                x64::ModRM().mod(1, 1).rm(x64::asOpBits(rDst)).reg(x64::asOpBits(rSrc))
                            });
                        } else RTASM_THROW_CE("unable to move mmx and gp registers", -1);
                    } else RTASM_THROW_CE("opcode mode not supported", -1);
                }

                static void emitADD(rtCompilationData & output, const StorageLoc & dst, const StorageLoc & src, const StorageLoc & out) {
                    if (dst.loc == StorageLoc::loc_t::reg && src.loc == StorageLoc::loc_t::reg) {
                        auto rDst = static_cast<x64::REG>(dst.reg);
                        auto rSrc = static_cast<x64::REG>(src.reg);
                        if (x64::isGp(rDst) && x64::isGp(rSrc)) {
                            write(output, {
                                x64::REX().w(1).r(x64::isGpRx(rDst)).b(x64::isGpRx(rSrc)),
                                0x3,
                                x64::ModRM().mod(1, 1).rm(x64::asOpBits(rSrc)).reg(x64::asOpBits(rDst))
                            });
                            if (dst != out) { emitMOV(output, out, dst); } // a very high quality assembly if this ever gets here
                        } else RTASM_THROW_CE("unable to move mmx and gp registers", -1);
                    } else RTASM_THROW_CE("opcode mode not supported", -1);
                }

            }

        }
    }
}

void rtasm::internal::compilation::compileMethod(rtAsmData & data, rtCompilationData & output) {
    StorageMgr storage { &data };
    auto isVoidRet = isVoid(data.methodDef->ret());

    for (usize i = 0; i < 2; ++i) {
        bool analyze = false;
        bool emit = false;
        switch (i) {
            case 0:
                analyze = true;
                storage.readOnly = false;
                break;
            case 1:
                emit = true;
                storage.readOnly = true;
                break;
                //default: RTASM_THROW_CE("nou", -1);
        }

        usize stackLocalSpace = 0;

        std::vector<usize> stackIdx2Offset;
        if (emit) {
            stackIdx2Offset = storage.stackIdx2Offset();
            if (!stackIdx2Offset.empty()) {
                stackLocalSpace = stackIdx2Offset[stackIdx2Offset.size() - 1];
                stackLocalSpace += x64::alignPad(call_conv::stackAlign, stackLocalSpace);
            }
        }

        // prologue
        if (emit) {
            write(output, {
                0x55, // push rbp
                0x48, 0x89, 0xe5 // mov rbp, rsp
            });
            if (stackLocalSpace != 0) { // allocate stack storage
                if (stackLocalSpace >= (1ull << 32)) { RTASM_THROW_CE("stack local size out of bounds", -1); }
                write(output, {
                    x64::REX().w(1), 0x81, x64::ModRM().reg(5).rm(x64::asOpBits(x64::REG::RSP)).value() // sub rsp ...
                });
                write(output, x64::asImmVal<std::uint32_t>((std::uint32_t) stackLocalSpace)); // ... <imm32>
            }
        }

        for (usize i = 0, l = data.ops.size(); i < l; ++i) {
            auto & op = data.ops[i];
            switch (op.op.m_code) {
                case OpCode::code_t::arg: {
                    auto dst = storage.getTmpStorage(op.output);
                    auto & src = storage.argLocs[op.op.m_data.u];
                    if (emit) emitMOV(output, dst, src);
                    break;
                }
                case OpCode::code_t::ret:
                    if (!isVoidRet) {
                        auto & dst = storage.retLoc;
                        auto src = storage.getTmpStorage(op.inputs[0].input);
                        if (dst != src) { if (emit) emitMOV(output, dst, src); }
                    }
                    if (emit) {
                        if (stackLocalSpace) {
                            write(output, {
                                x64::REX().w(1), 0x81, x64::ModRM().reg(0).rm(x64::asOpBits(x64::REG::RSP)).value() // add rsp ...
                            });
                            write(output, x64::asImmVal<std::uint32_t>((std::uint32_t) stackLocalSpace)); // ... <imm32>
                        }
                        write(output, { // epilogue
                            0x5d, // pop rbp
                            0xc3 // ret
                        });
                    }
                    break;
                case OpCode::code_t::add:
                    if (emit) emitADD(output, storage.getTmpStorage(op.inputs[0].input), storage.getTmpStorage(op.inputs[1].input), storage.getTmpStorage(op.output));
                    break;
                case OpCode::code_t::lbl:
                case OpCode::code_t::null: break; // nop
                default: RTASM_THROW_CE("opcode not implemented", i);
            }
        }
    }

}
