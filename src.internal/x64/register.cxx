#include <bitset>
#include <cstdint>

#include "register.hxx"

#include "tiny-test.h"

namespace rtasm {
    namespace internal {
        namespace x64 {

            const bool isNativeLe = [] () noexcept -> bool {
                union {
                    std::uint16_t a;
                    std::uint8_t b[2];
                } volatile a;
                a.a = 0x1234;
                return a.b[1] == 0x12;
            } ();

            TEST(ModRM) {
                ASSERT_EQ(ModRM().reg(2) == 0x10);
                ASSERT_EQ(ModRM().mod(1, 1).reg(2) == 0xd0);
                ASSERT_EQ(ModRM().mod(1, 1).rm(asOpBits(REG::RAX)).reg(asOpBits(REG::RCX)) == 0xc8);
            }

        }
    }
}
