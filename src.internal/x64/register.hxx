#ifndef HEADER_RTASM_INTERNAL_X64_REGISTER
#define HEADER_RTASM_INTERNAL_X64_REGISTER 1

#include <array>
#include <cstdint>

#include "../../include/rtasm/common.hpp"

namespace rtasm {
    namespace internal {
        namespace x64 {

            extern const bool isNativeLe;

            enum struct REG : std::uint8_t {
                null,

                RAX, RCX, RDX, RBX, RSP, RBP, RSI, RDI,
                R8, R9, R10, R11, R12, R13, R14, R15,

                XMM0, XMM1, XMM2, XMM3, XMM4, XMM5, XMM6, XMM7,
                XMM8, XMM9, XMM10, XMM11, XMM12, XMM13, XMM14, XMM15,

                gpBegin = RAX,
                gpEnd = R15 + 1,
                countGp = gpEnd - gpBegin,

                xmmBegin = XMM0,
                xmmEnd = XMM15 + 1,
                countXmm = xmmEnd - xmmBegin,

                count = countGp + countXmm
            };

            constexpr bool isGp(REG r) noexcept { return r >= REG::RAX && r <= REG::R15; }
            constexpr bool isGpRx(REG r) noexcept { return r >= REG::R8 && r <= REG::R15; }
            constexpr bool isXmm(REG r) noexcept { return r >= REG::XMM0 && r <= REG::XMM15; }
            constexpr bool validREG(usize r) noexcept { return r >= static_cast<usize>(REG::RAX) && r <= static_cast<usize>(REG::XMM15); }

            constexpr std::uint8_t asOpBits(REG r) noexcept {
                return isGp(r) ? ((isGpRx(r) ? (static_cast<std::uint8_t>(r) - static_cast<std::uint8_t>(REG::R8)) : static_cast<std::uint8_t>(r)) - static_cast<std::uint8_t>(REG::RAX)) :
                    (isXmm(r) ? ((static_cast<std::uint8_t>(r)) - static_cast<std::uint8_t>(REG::XMM0)) : 0xff); // not well understood - how to encode xmm8+? rex.w?
            }

            template<typename x>
            std::array<std::uint8_t, sizeof(x)> asImmVal(x value) noexcept {
                union {
                    x vX;
                    std::uint8_t v8[sizeof(x)];
                } v;
                v.vX = value;
                std::array<std::uint8_t, sizeof(x)> a;
                if (isNativeLe) {
                    for (int i = 0; i < sizeof(x); ++i) { a[i] = v.v8[i]; } // little endian memory layout - copy as is
                } else {
                    for (int i = 0; i < sizeof(x); ++i) { a[i] = v.v8[sizeof(x) - 1 - i]; } // be - invert order
                }
                return a;
            }

            struct REX {
                bool W;
                bool R;
                bool X;
                bool B;
                constexpr REX(bool w = false, bool r = false, bool x = false, bool b = false) noexcept : W(w), R(r), X(x), B(b) {}
                constexpr REX w(bool w) const noexcept { return REX(w, R, X, B); }
                constexpr REX r(bool r) const noexcept { return REX(W, r, X, B); }
                constexpr REX x(bool x) const noexcept { return REX(W, R, x, B); }
                constexpr REX b(bool b) const noexcept { return REX(W, R, X, b); }
                constexpr std::uint8_t value() const noexcept {
                    return (0x1 << 6) |
                        ((W ? 1 : 0) << 3) |
                        ((R ? 1 : 0) << 2) |
                        ((X ? 1 : 0) << 1) |
                        ((B ? 1 : 0) << 0);
                }
                constexpr operator std::uint8_t() const noexcept { return value(); }
            };

            struct ModRM {
                bool MOD[2];
                bool REG[3];
                bool RM[3];
                constexpr ModRM(bool mod0 = 0, bool mod1 = 0, bool reg0 = 0, bool reg1 = 0, bool reg2 = 0, bool rm0 = 0, bool rm1 = 0, bool rm2 = 0) noexcept :
                    MOD { mod0, mod1 }, REG { reg0, reg1, reg2 }, RM { rm0, rm1, rm2 } {}
                constexpr ModRM mod(bool mod0, bool mod1) const noexcept { return ModRM(mod0, mod1, REG[0], REG[1], REG[2], RM[0], RM[1], RM[2]); }
                constexpr ModRM reg(bool reg0, bool reg1, bool reg2) const noexcept { return ModRM(MOD[0], MOD[1], reg0, reg1, reg2, RM[0], RM[1], RM[2]); }
                constexpr ModRM reg(std::uint8_t reg) const noexcept { return ModRM::reg((reg >> 2) & 1, (reg >> 1) & 1, (reg >> 0) & 1); }
                constexpr ModRM rm(bool rm0, bool rm1, bool rm2) const noexcept { return ModRM(MOD[0], MOD[1], REG[0], REG[1], REG[2], rm0, rm1, rm2); }
                constexpr ModRM rm(std::uint8_t rm) const noexcept { return ModRM::rm((rm >> 2) & 1, (rm >> 1) & 1, (rm >> 0) & 1); }
                constexpr std::uint8_t value() const noexcept {
                    return
                        ((MOD[0] ? 1 : 0) << 7) |
                        ((MOD[1] ? 1 : 0) << 6) |
                        ((REG[0] ? 1 : 0) << 5) |
                        ((REG[1] ? 1 : 0) << 4) |
                        ((REG[2] ? 1 : 0) << 3) |
                        ((RM[0] ? 1 : 0) << 2) |
                        ((RM[1] ? 1 : 0) << 1) |
                        ((RM[2] ? 1 : 0) << 0);
                }
                constexpr operator std::uint8_t() const noexcept { return value(); }
            };

        }
    }
}

#endif
