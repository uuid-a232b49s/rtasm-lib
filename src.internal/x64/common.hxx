#ifndef HEADER_RTASM_INTERNAL_X64_COMMON
#define HEADER_RTASM_INTERNAL_X64_COMMON 1

#include "../../include/rtasm/common.hpp"
#include "../../include/rtasm/compilation.hpp"

namespace rtasm {
    namespace internal {
        namespace x64 {

            enum struct StorageType {
                null, // unknown
                stack, // undefined type - cant be placed into a register
                integral, // integral type - can be placed into GPR
                floating // floating type - can be placed into XMM
            };

            inline StorageType storageType(const TypeDefMeta & tm) noexcept {
                if (tm.indirection > 0) { return StorageType::integral; }
                switch (tm.type->id()) {
                    case ITypeDef::UInt::Id:
                    case ITypeDef::SInt::Id: return StorageType::integral;
                    case ITypeDef::Float::Id: return StorageType::floating;
                    case ITypeDef::Extern::Id: return StorageType::stack;
                    default: return StorageType::null;
                }
            }

            inline constexpr usize alignPad(usize typeAlign, usize currentSize) noexcept {
                return (currentSize % typeAlign) == 0 ? 0 : typeAlign - (currentSize % typeAlign);
            }

        }
    }
}

#endif
