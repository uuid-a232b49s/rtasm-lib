#include "../../src/exception.hxx"

#include "../x64/common.hxx"
#include "../x64/register.hxx"
#include "../internal/call-conv.hxx"

namespace {
    using namespace rtasm::internal::call_conv;

    static RegInfo RegInfo0(bool varSafe, bool calleePreserved) noexcept {
        RegInfo a;
        a.calleePreserved = calleePreserved;
        a.varSafe = varSafe;
        return a;
    }

}

// system v
// https://gitlab.com/x86-psABIs/x86-64-ABI
// Figure 3.4: Register Usage

namespace rtasm {
    namespace internal {
        namespace call_conv {

            RegInfo regInfo(usize registerId) {
                if (x64::isXmm(static_cast<x64::REG>(registerId))) { return RegInfo0(true, false); }
                switch (static_cast<x64::REG>(registerId)) {
                    case x64::REG::RAX: 
                    case x64::REG::RCX:
                    case x64::REG::RDX:
                    case x64::REG::RSI:
                    case x64::REG::RDI:
                    case x64::REG::R8:
                    case x64::REG::R9:
                    case x64::REG::R10:
                    case x64::REG::R11: return RegInfo0(true, false);
                    case x64::REG::RBX: // maybe this 1 is var-safe
                    case x64::REG::RSP:
                    case x64::REG::RBP: return RegInfo0(false, true);
                    case x64::REG::R12:
                    case x64::REG::R13:
                    case x64::REG::R14:
                    case x64::REG::R15: return RegInfo0(true, true);
                    default: RTASM_THROW_CE("illegal register id", -1);
                }
            }

            void getArgLayout(const compilation::rtMethod * m, StorageLoc * retLoc, StorageLoc * argLocs) {
                {
                    auto & ret = m->m_sig[0];
                    switch (x64::type(ret)) {
                        case x64::Type::integral:
                            retLoc->loc = StorageLoc::loc_t::reg;
                            retLoc->reg = static_cast<usize>(x64::REG::RAX);
                            break;
                        case x64::Type::floating:
                            retLoc->loc = StorageLoc::loc_t::reg;
                            retLoc->reg = static_cast<usize>(x64::REG::XMM0);
                            break;
                        case x64::Type::void_:
                            retLoc->loc = StorageLoc::loc_t::null;
                            break;
                        default: RTASM_THROW_CE("unsupported return type", -1);
                    }
                }
                {
                    auto argc = m->m_sig.size() - 1;
                    usize nextI = 0;
                    usize nextF = 0;
                    usize nextArgStackIdx = 0;
                    for (usize i = 0; i < argc; ++i) {
                        auto & arg = m->m_sig[i + 1];
                        auto & argLoc = argLocs[i];
                        switch (x64::type(arg)) {
                            case x64::Type::integral:
                                if (nextI < 6) {
                                    x64::REG r;
                                    switch (nextI) {
                                        case 0: r = x64::REG::RDI; break;
                                        case 1: r = x64::REG::RSI; break;
                                        case 2: r = x64::REG::RDX; break;
                                        case 3: r = x64::REG::RCX; break;
                                        case 4: r = x64::REG::R8; break;
                                        case 5: r = x64::REG::R9; break;
                                        default: RTASM_THROW_CE("unexpected state", -1);
                                    }
                                    argLoc.loc = StorageLoc::loc_t::reg;
                                    argLoc.reg = static_cast<usize>(r);
                                } else {
                                    argLoc.loc = StorageLoc::loc_t::stack;
                                    argLoc.stackIdx = nextArgStackIdx++;
                                }
                                ++nextI;
                                break;
                            case x64::Type::floating:
                                if (nextF < 6) {
                                    x64::REG r;
                                    switch (nextF) {
                                        case 0: r = x64::REG::XMM2; break;
                                        case 1: r = x64::REG::XMM3; break;
                                        case 2: r = x64::REG::XMM4; break;
                                        case 3: r = x64::REG::XMM5; break;
                                        case 4: r = x64::REG::XMM6; break;
                                        case 5: r = x64::REG::XMM7; break;
                                        default: RTASM_THROW_CE("unexpected state", -1);
                                    }
                                    argLoc.loc = StorageLoc::loc_t::reg;
                                    argLoc.reg = static_cast<usize>(r);
                                } else {
                                    argLoc.loc = StorageLoc::loc_t::stack;
                                    argLoc.stackIdx = nextArgStackIdx++;
                                }
                                ++nextF;
                                break;
                            default: RTASM_THROW_CE("unsupported arg type", -1);
                        }
                    }
                }
            }

        }
    }
}
