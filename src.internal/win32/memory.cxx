#include "../internal/memory.hxx"

#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#include <Memoryapi.h>

namespace {

    union header_t {
        SIZE_T m_size;
        unsigned char m_pad_[64];
    };

}

namespace rtasm {
    namespace internal {
        namespace memory {

            void * allocate(unsigned long long size) noexcept {
                auto m = VirtualAlloc(NULL, sizeof(header_t) + size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
                if (!m) { return nullptr; }
                auto & h = *static_cast<header_t *>(m);
                h.m_size = sizeof(header_t) + size;
                return static_cast<unsigned char *>(m) + sizeof(header_t);
            }

            bool deallocate(void * m_) noexcept {
                void * m = static_cast<unsigned char *>(m_) - sizeof(header_t);
                return VirtualFree(m, 0, MEM_RELEASE) ? true : false;
            }

            bool protect(void * m_) noexcept {
                DWORD x_;
                void * m = static_cast<unsigned char *>(m_) - sizeof(header_t);
                return VirtualProtect(m_, static_cast<header_t *>(m)->m_size, PAGE_EXECUTE_READWRITE, &x_) ? true : false;
            }

        }
    }
}
