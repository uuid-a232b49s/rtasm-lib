#include "../internal/memory.hxx"

#include <sys/mman.h>

namespace {

    union header_t {
        size_t m_size;
        unsigned char m_pad_[64];
    };

}

namespace rtasm {
    namespace internal {
        namespace memory {

            void * allocate(unsigned long long size) noexcept {
                auto m = mmap(nullptr, sizeof(header_t) + size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
                if (m == MAP_FAILED) { return nullptr; }
                auto & h = *static_cast<header_t *>(m);
                h.m_size = sizeof(header_t) + size;
                return static_cast<unsigned char *>(m) + sizeof(header_t);
            }

            bool deallocate(void * m_) noexcept {
                void * m = static_cast<unsigned char *>(m_) - sizeof(header_t);
                mprotect(m, sizeof(header_t), PROT_READ | PROT_WRITE);
                return munmap(m, static_cast<header_t *>(m)->m_size) == 0;
            }

            bool protect(void * m_) noexcept {
                void * m = static_cast<unsigned char *>(m_) - sizeof(header_t);
                return mprotect(m, static_cast<header_t *>(m)->m_size, PROT_EXEC) == 0;
            }

        }
    }
}
