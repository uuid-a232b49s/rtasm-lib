#ifndef HEADER_RTASM_INTERNAL_CALLCONV
#define HEADER_RTASM_INTERNAL_CALLCONV 1

#include "storage-loc.hxx"

#include "../include/rtasm/common.hpp"
#include "../src/compilation.hxx"

namespace rtasm {
    namespace internal {
        namespace call_conv {

            struct RegInfo {
                bool varSafe = false; // safe to use as a variable storage
                bool calleePreserved = false; // whether the register is preserved by the callee
            };

            extern const usize stackAlign;
            extern RegInfo regInfo(usize reg);
            extern void getArgLayout(const compilation::rtAsmData *, StorageLoc * retLoc, StorageLoc * argLocs);

        }
    }
}

#endif
