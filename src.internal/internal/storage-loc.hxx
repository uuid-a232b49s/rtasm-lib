#ifndef HEADER_RTASM_INTERNAL_STORAGELOC
#define HEADER_RTASM_INTERNAL_STORAGELOC 1

#include "../../include/rtasm/common.hpp"

namespace rtasm {
    namespace internal {

        // represents a logical storage location for a value
        struct StorageLoc {
            enum struct loc_t {

                // undefined location
                null,

                // value located in a register
                reg,

                // value located on a stack
                stack

            } loc = loc_t::null;
            union {
                usize reg; // register id
                usize stackIdx; // index offset from stack beginning
            };

            friend bool operator==(const StorageLoc & a, const StorageLoc & b) noexcept {
                if (a.loc != b.loc) { return false; }
                switch (a.loc) {
                    case loc_t::reg: return a.reg == b.reg;
                    case loc_t::stack: return a.stackIdx == b.stackIdx;
                    default: return false;
                }
            }

            friend bool operator!=(const StorageLoc & a, const StorageLoc & b) noexcept { return !(a == b); }

        };

    }
}

#endif
