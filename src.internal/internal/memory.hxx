#ifndef HEADER_RTASM_INTERNAL_MEMORY
#define HEADER_RTASM_INTERNAL_MEMORY 1

namespace rtasm {
    namespace internal {
        namespace memory {

            void * allocate(unsigned long long) noexcept;
            bool deallocate(void *) noexcept;
            bool protect(void *) noexcept;

            struct deleter_t {
                void operator()(void * m) const noexcept {
                    deallocate(m);
                }
            };

        }
    }
}

#endif
